// import coursesData from "../data/coursesData";
import  { useState, useEffect } from 'react'
import CourseCard from "../components/CourseCard";
import React from "react";

export default function Courses(){
    //Checks to see if the mock data was captured.
    // console.log(coursesData)
    const [courses,setCourses] = useState([])

    useEffect(()=>{
        fetch(`${process.env.REACT_APP_API_URL}/courses/`)
        .then(res => res.json())
        .then(data => {
            console.log(data)
            setCourses(data.map(course => {
                return(
                    <CourseCard key={course._id} courseProp={course}/>
                )
            }))
        })
    },[])


    /* const courses = coursesData.map(course=>{
        return(
            <CourseCard key={course.id} courseProp={course}/>
        )
    }) */

    return(
        <>
            <h1>Courses</h1>
            {courses}
        </>
        
    )
}