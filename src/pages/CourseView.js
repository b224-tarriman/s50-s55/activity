import { useState,useEffect, useContext } from 'react'
import { Container,Card,Button,Row,Col } from 'react-bootstrap'
import { useParams,useNavigate,Link } from 'react-router-dom'
import Swal from 'sweetalert2'
import UserContext from '../UserContext'

const CourseView = () => {
    const { user } = useContext(UserContext)
    
    const navigate = useNavigate();
    // The "useParams" is a hook which allows us to retrieve the courseId passed via the URL parameter.
    const { courseId } = useParams()

    const [name,setName]=useState("")
    const [description,setDescription] = useState("")
    const [price,setPrice]=useState(0)

    const enroll = (courseId) => {
        fetch(`${process.env.REACT_APP_API_URL}/users/enroll`,{
            method:"POST",
            headers:{
                'Content-Type':'application/json',
                Authorization:`Bearer ${localStorage.getItem('token')}`
            },
            body: JSON.stringify({
                courseId:courseId
            })
        })
        .then(res=>res.json())
        .then(data=>{
            console.log(data)

            if(data === true){
                Swal.fire({
                    title:"Successfully enrolled",
                    icon:"success",
                    text:"You have successfully enrolled in this course"
                })

                navigate("/courses")

            }else{
                Swal.fire({
                    title:"Something went wrong",
                    icon:"error",
                    text:"Please try again."
                })
            }
        })
    }

    useEffect(()=>{
        console.log(courseId)
        fetch(`${process.env.REACT_APP_API_URL}/courses/${courseId}`)
        .then(res=>res.json())
        .then(data=>{
            console.log(data)
            const {name,description,price} = data
            setName(name)
            setDescription(description)
            setPrice(price)
        })
    },[courseId])
  return (
    <Container>
         <Row className='mt-3 mb-3'>
            <Col lg={{span:6,offset:3}}>
                <Card>
                    <Card.Body className='text-center'>
                        <Card.Title>{name}</Card.Title>
                        <Card.Subtitle>Description:</Card.Subtitle>
                        <Card.Text>
                        {description}
                        </Card.Text>
                        <Card.Subtitle className="mb-2">Price:</Card.Subtitle>
                        <Card.Text>
                        PhP {price}
                        </Card.Text>
                        <Card.Subtitle>Class Schedule:</Card.Subtitle>
                        <Card.Subtitle>8:00 AM - 5:00 PM</Card.Subtitle>
                    </Card.Body>
                    {
                        (user.id !== null) ?
                        <Button variant='primary' onClick={()=>enroll(courseId)}>Enroll</Button>
                        :
                        <Button className='btn btn-danger' as={Link} to="/login">Log in to Enroll</Button>

                        
                    }
                </Card>                
            </Col>
        </Row>
    </Container>
  )
}

export default CourseView
