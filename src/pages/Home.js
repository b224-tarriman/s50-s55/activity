import Banner from '../components/Banner'
import Highlights from '../components/Highlights'
// import CourseCard from '../components/CourseCard'

export default function Home() {

    const data = {
        title:"Zuitt Coding Bootcamp",
        description: "Opportunities for everyone, everywhere.",
        destination:"/courses",
        label: "Enroll Now!"
    }
    return (
        <>
            <Banner prop={data}/>
            <Highlights/>
            {/* <CourseCard/> */}
        </>
    )
}