
import Banner from "../components/Banner";

export default function notFound(){
    
    const data = {
        title:"Error 404 - Page Not Found",
        description: "The page you are looking for cannot be found",
        destination:"/",
        label: "Back to Home"
    }
    return(
        <>
            {<Banner prop={data}/>}
        </>
        
    )
}