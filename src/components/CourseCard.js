import { useState, useEffect } from 'react'
import { Row, Col, Card, Button } from 'react-bootstrap'
import { Link } from 'react-router-dom';


export default function CourseCard({courseProp}) {
    
    // console.log(courseProp)
    /* 
        Use the state hook for this component to be able to store its state.
        States are used to keep track of the information related to individual components

        Syntax:
            const [getter,setter] = useState(initialGetterValue)
    */

    const [count,setCount] = useState(0);
    const [seats,setSeats] = useState(30)
    // console.log(useState(0))
   /*  function enroll(){
        
        if(count===30 && seats === 0){
        alert("No more seats left!")
        }else{
            setSeat(seats-1)
            console.log(seats)
            setCount(count + 1)
            console.log(count)
        }
        
    } */
    function enroll() {
		if(count < 30) {
			setCount(count + 1)
			// console.log('Enrollee: ' + count)
			setSeats(seats - 1)
			// console.log('Seats: ' + seats)
		 } //else {
			// alert("No more seats available.")
		// }
	}

    useEffect(()=>{
        if(seats === 0){
            alert("No more seats available.")
        }
    },[seats])
    const { name,description,price,_id } = courseProp
    return(
        <Row className='mt-3 mb-3'>
            <Col xs={12} md={12} lg={6}>
                <Card className='cardCourse p-3'>
                    <Card.Body>
                        <Card.Title>{name}</Card.Title>
                        <Card.Subtitle className="mb-2">Description:</Card.Subtitle>
                        <Card.Text>
                        {description}
                        </Card.Text>
                        <Card.Subtitle className="mb-2">Price:</Card.Subtitle>
                        <Card.Text>
                        PhP {price}
                        </Card.Text>
                    </Card.Body>
                    <Button className="enrollBtn bg-primary" as={Link} to ={`/courses/${_id}`}>Details</Button>
                </Card>                
            </Col>
        </Row>
    )
}