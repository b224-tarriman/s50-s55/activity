import {Row,Col,Button} from 'react-bootstrap'
import { Link } from 'react-router-dom'

export default function Banner({prop}){
    
   const {title,description,destination,label} = prop
    return(

        <Row>
            <Col className='p-5'>
            <h1>{title}</h1>
            <p>{description}</p>
            <Button as={Link} to={destination}>{label}</Button>
            </Col>
        </Row>
    )
}